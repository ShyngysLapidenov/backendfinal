FROM python
ENV THREAD_STACK_SIZE 10M
ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONNOUSERSITE 1
ENV PIP_NO_CACHE_DIR off

WORKDIR /backendfinal
COPY /requirements.txt ./
RUN ulimit -s unlimited
RUN pip install --upgrade pip --progress-bar off && \
    pip install --no-cache-dir --disable-pip-version-check --progress-bar off -r requirements.txt
COPY . .

EXPOSE 8000
CMD ["python", "manage.py", "runserver"]
